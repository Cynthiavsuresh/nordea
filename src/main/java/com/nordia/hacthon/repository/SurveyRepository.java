package com.nordia.hacthon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nordia.hacthon.entity.SurveyDetails;
@Repository
public interface SurveyRepository extends JpaRepository<SurveyDetails, Integer> {

}
