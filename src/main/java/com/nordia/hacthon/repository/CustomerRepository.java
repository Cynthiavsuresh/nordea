package com.nordia.hacthon.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nordia.hacthon.entity.Customer;
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
	
	Optional<Customer> findByFirstNameIgnoreCaseAndAddressIgnoreCase(String firstName, String address);
	Optional<Customer> findByAddressIgnoreCase(String address);

}
