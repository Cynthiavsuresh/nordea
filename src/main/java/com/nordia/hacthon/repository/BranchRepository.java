package com.nordia.hacthon.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nordia.hacthon.entity.BranchDetails;
@Repository
public interface BranchRepository extends JpaRepository<BranchDetails, Integer> {
	Optional<BranchDetails> findByBranchId(int branchId);

	Object findByAddress(String string);

}
