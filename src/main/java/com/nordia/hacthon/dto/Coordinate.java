package com.nordia.hacthon.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Coordinate {
    private double latitude;
    private double longitude;
}
