package com.nordia.hacthon.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Surveydetailsdto {
	private long latitude;
	private long longitude;
}
