package com.nordia.hacthon.excpetion;

public class EntityFetchingException extends Exception {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EntityFetchingException(String message) {
	        super(message);
	    }

}
