package com.nordia.hacthon.excpetion;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NordiaAppGlobalException  extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	private String message;
	private HttpStatus httpStatus;
	
	public NordiaAppGlobalException(String message) {
		super(message);
	}

	

}
