package com.nordia.hacthon.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nordia.hacthon.entity.SurveyDetails;
import com.nordia.hacthon.excpetion.EntityFetchingException;
import com.nordia.hacthon.service.SurveyService;

import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
public class SurveyController {
	@Autowired
	private SurveyService surveyService;
	
	@Autowired
    private SurveyService service;

	@PostMapping("/entities/csv")
	   public ResponseEntity<?> generateCSV(HttpServletResponse response) throws EntityFetchingException {
        try {
            service.generateCSV();
            // Set response headers
            response.setContentType("text/csv");
            response.setHeader("Content-Disposition", "attachment; filename=bank.csv");
            // Write file content to response
            Files.copy(Paths.get("bank.csv"), response.getOutputStream());
            response.getOutputStream().flush();
            return ResponseEntity.ok().build();
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to generate CSV");
        }
    }
	
	@GetMapping("/{address}")
	public ResponseEntity<List<SurveyDetails>> getAllCustomer(@RequestParam int bracnhId,@PathVariable String address){
			return new ResponseEntity<>(surveyService.getAll(bracnhId,address),HttpStatus.OK);
		}

}
