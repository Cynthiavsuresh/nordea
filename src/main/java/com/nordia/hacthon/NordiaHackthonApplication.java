package com.nordia.hacthon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NordiaHackthonApplication {

	public static void main(String[] args) {
		SpringApplication.run(NordiaHackthonApplication.class, args);
	}

}
