package com.nordia.hacthon.service.Impl;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.nordia.hacthon.dto.ApiResponse;
import com.nordia.hacthon.dto.CustomerDto;
import com.nordia.hacthon.entity.BranchDetails;
import com.nordia.hacthon.entity.Customer;
import com.nordia.hacthon.excpetion.ResourceConflictExists;
import com.nordia.hacthon.excpetion.ResourceNotFound;
import com.nordia.hacthon.repository.BranchRepository;
import com.nordia.hacthon.repository.CustomerRepository;
import com.nordia.hacthon.service.CustomerService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

	private final CustomerRepository customerRepository;
	private final BranchRepository branchRepository;

	@Override
	public ApiResponse register(int branchId,CustomerDto customerDto) {
		Optional<BranchDetails> branchId2 = branchRepository.findByBranchId(branchId);
		if(branchId2.isEmpty()) {
			throw new ResourceNotFound("Branch Id not present");
		}

		Optional<Customer> customer = customerRepository
				.findByFirstNameIgnoreCaseAndAddressIgnoreCase(customerDto.getFirstName(), customerDto.getAddress());

		if (customer.isPresent()) {
			throw new ResourceConflictExists("Customer already Register in this address ");
		}
		Customer customer1 = Customer.builder().firstName(customerDto.getFirstName())
				.lastName(customerDto.getLastName()).address(customerDto.getAddress()).gender(customerDto.getGender())
				.branch(branchId2.get())
				.build();
		customerRepository.save(customer1);

		return ApiResponse.builder().code(201l).message("Customer register succefully").build();
	}

	
}
