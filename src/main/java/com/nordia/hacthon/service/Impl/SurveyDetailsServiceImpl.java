package com.nordia.hacthon.service.Impl;

import java.io.FileWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.nordia.hacthon.entity.BranchDetails;
import com.nordia.hacthon.entity.Customer;
import com.nordia.hacthon.entity.SurveyDetails;
import com.nordia.hacthon.excpetion.ResourceNotFound;
import com.nordia.hacthon.repository.BranchRepository;
import com.nordia.hacthon.repository.CustomerRepository;
import com.nordia.hacthon.repository.SurveyRepository;
import com.nordia.hacthon.service.SurveyService;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class SurveyDetailsServiceImpl implements SurveyService {

	private final SurveyRepository surveyRepository;
	private final BranchRepository branchRepository;
	private final CustomerRepository customerRepository;

	@Override
	public List<SurveyDetails> getAllEntities() {
		return surveyRepository.findAll();
	}

	@Override
	public void generateCSV() {
		List<SurveyDetails> entities = getAllEntities();
		try (Writer writer = new FileWriter("bank.csv")) {
			Field[] fields = SurveyDetails.class.getDeclaredFields();

			// Write headers

			writer.write(Arrays.stream(fields)

					.map(Field::getName)

					.collect(Collectors.joining(",")) + "\n");
			StatefulBeanToCsv<SurveyDetails> beanToCsv = new StatefulBeanToCsvBuilder<SurveyDetails>(writer)
					.withSeparator(',').withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
					.withSeparator(CSVWriter.DEFAULT_SEPARATOR).build();
			beanToCsv.write(entities);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<SurveyDetails> getAll(int bracnhId, String address) {
		Optional<BranchDetails> branchDetails = branchRepository.findByBranchId(bracnhId);
		if (branchDetails.isEmpty()) {
			throw new ResourceNotFound("Branch not found");
		}

		BranchDetails details = branchDetails.get();

		Optional<Customer> customer = customerRepository.findByAddressIgnoreCase(address);
		if (customer.isEmpty()) {
			throw new ResourceNotFound("In this Address customer not register");
		}
		Customer customer2 = customer.get();
		if (customer2.getAddress().equalsIgnoreCase(address)) {
			double EARTH_RADIUS_KM = 6371.0;
			double dLat = Math.toRadians(details.getBranchLatitude() - customer2.getCustomerLatitude());
			double dLng = Math.toRadians(details.getBranchLongitude() - customer2.getCustomerLongitude());

			double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
					+ Math.cos(Math.toRadians(customer2.getCustomerLatitude()))
							* Math.cos(Math.toRadians(details.getBranchLatitude())) * Math.sin(dLng / 2)
							* Math.sin(dLng / 2);

			double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

			double distance = EARTH_RADIUS_KM * c;
			SurveyDetails surveyDetails = SurveyDetails.builder().BranchID(bracnhId).CustomerID(customer2.getCustomId())
					.Distance(distance).build();

			surveyRepository.save(surveyDetails);
		}
		return surveyRepository.findAll();

	}
	

}
