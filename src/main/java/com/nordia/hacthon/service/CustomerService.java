package com.nordia.hacthon.service;

import com.nordia.hacthon.dto.ApiResponse;
import com.nordia.hacthon.dto.CustomerDto;

public interface CustomerService {

	ApiResponse register( int branschId,CustomerDto customerDto);

}
