package com.nordia.hacthon.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.nordia.hacthon.dto.ApiResponse;
import com.nordia.hacthon.dto.CustomerDto;
import com.nordia.hacthon.service.CustomerService;

@ExtendWith(SpringExtension.class)
public class CustomerControllerTest {
	
	@InjectMocks
	private CustomerController customerController;
	
	@Mock
	private CustomerService customerService;
	
	@Test
	void registrationSuccess() {
		int branchId = 1;
		CustomerDto customerDto = CustomerDto.builder().firstName("Praveen").address("Bangalore").build();
		ApiResponse api = ApiResponse.builder().code(201l).build();
		Mockito.when(customerService.register(branchId,customerDto)).thenReturn(api);
		ResponseEntity<ApiResponse> register = customerController.register(branchId,customerDto);
		assertEquals(201, register.getBody().getCode());
		
	}

}
