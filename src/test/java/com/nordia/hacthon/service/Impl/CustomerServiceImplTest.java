package com.nordia.hacthon.service.Impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.nordia.hacthon.dto.ApiResponse;
import com.nordia.hacthon.dto.CustomerDto;
import com.nordia.hacthon.entity.BranchDetails;
import com.nordia.hacthon.entity.Customer;
import com.nordia.hacthon.excpetion.ResourceConflictExists;
import com.nordia.hacthon.excpetion.ResourceNotFound;
import com.nordia.hacthon.repository.BranchRepository;
import com.nordia.hacthon.repository.CustomerRepository;

@ExtendWith(SpringExtension.class)
class CustomerServiceImplTest {
	@InjectMocks
	private CustomerServiceImpl customerServiceImpl;
	@Mock
	private CustomerRepository customerRepository;
	@Mock
	private BranchRepository branchRepository;

	@Test
	void customerRegisterSuccefull() {
		int brancgId = 1;
		BranchDetails branchDetails = BranchDetails.builder().branchId(brancgId).build();
		Mockito.when(branchRepository.findByBranchId(brancgId)).thenReturn(Optional.of(branchDetails));
		Customer customer = Customer.builder().firstName("Praveen").address("Bangalore").build();
		Mockito.when(customerRepository.findByFirstNameIgnoreCaseAndAddressIgnoreCase("Praveen", "Bangalore"))
				.thenReturn(Optional.empty());
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		CustomerDto customerDto = CustomerDto.builder().firstName("Praveen").address("Bangalore").build();

		ApiResponse register = customerServiceImpl.register(brancgId, customerDto);
		assertEquals("Customer register succefully", register.getMessage());
	}

	@Test
	void customerRegisterUnSucce() {

		int brancgId = 1;
		BranchDetails branchDetails = BranchDetails.builder().branchId(brancgId).build();
		Mockito.when(branchRepository.findByBranchId(brancgId)).thenReturn(Optional.of(branchDetails));
		Customer customer = Customer.builder().firstName("Praveen").address("Bangalore").build();

		Mockito.when(customerRepository.findByFirstNameIgnoreCaseAndAddressIgnoreCase("Praveen", "Bangalore"))
				.thenReturn(Optional.of(customer));
		CustomerDto customerDto = CustomerDto.builder().firstName("Praveen").address("Bangalore").build();
		assertThrows(ResourceConflictExists.class, () -> customerServiceImpl.register(brancgId, customerDto));
	}

	@Test
	void customerRegistorBranchIdNotPresent() {
		int brancgId = 3;
//		BranchDetails branchDetails = BranchDetails.builder().branchId(4).build();
		Mockito.when(branchRepository.findByBranchId(brancgId)).thenReturn(Optional.empty());
//		Customer customer = Customer.builder().firstName("Praveen").address("Bangalore").build();
//
//		Mockito.when(customerRepository.findByFirstNameIgnoreCaseAndAddressIgnoreCase("Praveen", "Bangalore"))
//				.thenReturn(Optional.of(customer));
		CustomerDto customerDto = CustomerDto.builder().firstName("Praveen").address("Bangalore").build();
		assertThrows(ResourceNotFound.class, () -> customerServiceImpl.register(brancgId, customerDto));
	}

}
