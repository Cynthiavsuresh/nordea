package com.nordia.hacthon.service.Impl;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.nordia.hacthon.entity.SurveyDetails;
import com.nordia.hacthon.excpetion.ResourceNotFound;
import com.nordia.hacthon.repository.BranchRepository;
import com.nordia.hacthon.repository.CustomerRepository;

import com.nordia.hacthon.repository.SurveyRepository;

@ExtendWith(SpringExtension.class)
class SurveyDetailsServiceImplTest {
	
	@InjectMocks
	private SurveyDetailsServiceImpl surveyDetailsServiceImpl;
	@Mock
	private SurveyRepository surveyRepository;
	@Mock
	private BranchRepository branchRepository;
	@Mock
	private CustomerRepository customerRepository;
	
	@Test
	void getall() {
		String add="add";
		Mockito.when(branchRepository.findByBranchId(1)).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class, ()-> surveyDetailsServiceImpl.getAll(1, add));
	}
    
    @Test
    public void testGenerateCSV() throws Exception {
        // Mock entity data
        List<SurveyDetails> mockEntities = Arrays.asList(
                SurveyDetails.builder().SurveyID(1).CustomerID(101).BranchID(201).Distance(10.5).build(),
                SurveyDetails.builder().SurveyID(2).CustomerID(102).BranchID(202).Distance(20.5).build()
        );
 
        // Mock repository behavior
        when(surveyRepository.findAll()).thenReturn(mockEntities);
 
        // Call the method to test
        surveyDetailsServiceImpl.generateCSV();
 
        // Verify that findAll method of repository is called
        verify(surveyRepository).findAll();
 
        // You can also further assert the behavior as needed
    }
}
